# Introduction to Functional Programming

This contains the slides, glossary, and sample code for a presentation
aiming to introduce you to the basics of functional programming. In
short, the presentation could be summarized as:


In this introduction to the spirit and terminology of Functional
Programming (FP), you'll be introduced to the rationale, feel, and
basic theory of FP. This includes:

* A definition of FP
* Motivation: why FP matters
* Working together: prominent FP languages
* Higher-order functions
  - Compose towards greater reuse
  - Eliminate most loops: maps, folds, and filters
* How types guide the flow of our data, transforms, and abstractions
* Where to go from here - specializing FP to *your* interests

## Contents

```
├── build.sh  -- build the talk using pandoc
├── docs      
│   ├── glossary.md    -- terms used in presentation
│   └── languages.md   -- links to languages referenced
├── my.beamer          -- LaTeX style sheet
├── README.md
├── slides.md
└── src
    ├── HigherOrder.hs -- higher-order function samples
    └── Types.hs       -- typechecking samples
```

## Building

Requirements:

* [GHC](https://github.com/bitemyapp/learnhaskell#getting-started) Haskell Compiler
* [Pandoc](http://johnmacfarlane.net/pandoc/)
* [LaTex](https://www.tug.org/texlive/)

Command:

```
$ ./build.sh
```

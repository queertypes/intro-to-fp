module Types (
  Weekday,
  nextDay,
  nextDay',

  Switch,
  toggle,
  toggle',

  Age,
  getOlder,
  x
) where

-- sum, or "one-of" types
data Weekday = M | T | W | R | F deriving Show

-- pattern matching, compile-time checked!
nextDay :: Weekday -> Weekday
nextDay d = case d of
    M -> T
    T -> W
    W -> R
    R -> F
    F -> M

-- type warning: patterns not exhausted
{-
Warning: Pattern match(es) are non-exhaustive
In a case alternative: Patterns not matched: F
-}
nextDay' :: Weekday -> Weekday
nextDay' d = case d of
    M -> T
    T -> W
    W -> R
    R -> F

-- product types: multiply possibilities!
data Switch = Switch Bool Bool deriving Show

toggle :: Switch -> Switch
toggle (Switch True True) = Switch False False
toggle (Switch False True) = Switch True False
toggle (Switch True False) = Switch False True
toggle (Switch False False) = Switch True True

-- type warning: patterns not exhausted
{-
Warning: Pattern match(es) are non-exhaustive
In an equation for ‘toggle'’:
    Patterns not matched: Switch False False
-}
toggle' :: Switch -> Switch
toggle' (Switch True True) = Switch False False
toggle' (Switch False True) = Switch True False
toggle' (Switch True False) = Switch False True

newtype Age = Age Int

getOlder :: Age -> Age
getOlder (Age n) = Age (n + 1)

x :: Age
x = Age 1
-- type error: can't add Age and Int
-- y = x + 1
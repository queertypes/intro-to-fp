module HigherOrder (
  map',
  filter',
  foldr',
  compose,

  sum',
  product',
  and',
  or'
) where

-- hide some definitions to avoid collisions
import Prelude hiding (map, filter, foldr, sum, or, and, product)

map' :: (a -> b) -> [a] -> [b]
map' f xs = case xs of
  []      -> []
  (x:xs') -> f x : map' f xs'

filter' :: (a -> Bool) -> [a] -> [a]
filter' p xs = case xs of
  []      -> []
  (x:xs') -> if p x
             then x : filter' p xs'
             else filter' p xs'

foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' f z xs = case xs of
  [] -> z
  (x:xs') -> f x (foldr' f z xs')

compose :: (a -> b) -> (b -> c) -> a -> c
compose f g = g . f

sum' :: Num a => [a] -> a
sum' xs = foldr' (+) 0 xs

product' :: Num a => [a] -> a
product' xs = foldr' (*) 1 xs

and' :: [Bool] -> Bool
and' xs = foldr' (&&) True xs

or' :: [Bool] -> Bool
or' xs = foldr' (||) False xs
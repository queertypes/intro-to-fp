# Glossary

An abridged version of all definitions given in the presentation.

## Expression

One of three things:

* Variable/Value: `1`, `x`, `"cat"`
* Abstraction: binding an expression to a name - `let x = 1`
* Application: calling a function: `f 10`

## Immutability

* Once defined, values cannot be changed

## Functions

* A map from input to output

## Idempotent

* Given the same input, yield the same output

## Effect

* An action performed by a function that changes the execution environment
    * Examples: read/write disk, mutate a memory location, log, print

## Purity

* The absence of side-effects
* All effects are explicitly tracked

## Referential Transparency

* The ability to exchange an expression for its value without altering
  the meaning of a program

## Type

* The shape of values
* Checked before run-time/at-compile time

## Type Inference

* The ability for a compiler to infer types

## Type Error

* A mismatch of types for an expression
* Usually reported at compile-time by languages

## Sum Type

* "one-of" types: a given value is "one-of" the given types

```haskell
data Weekday = M | T | W | R | F
```

## Product Type

* "each of" types: a value consists of each of the types provided

```haskell
data Switch = Switch Bool Bool
```

## Higher Order Functions

* Functions that take functions as arguments or return functions

## Function: Map

* Apply a function to a collection of elements

```haskell
map' :: (a -> b) -> [a] -> [b]
map' f xs = case xs of
  []      -> []
  (x:xs') -> f x : map' f xs'
```

## Function: Filter

* Extract elements of interest from a collection

```haskell
filter' :: (a -> Bool) -> [a] -> [a]
filter' p xs = case xs of
  []      -> []
  (x:xs') -> if p x
             then x : filter' p xs'
             else filter' p xs'
```

## Function: Fold

* Summarize a collection by merging elements down to a value

```haskell
foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' f z xs = case xs of
  [] -> z
  (x:xs') -> f x (foldr' f z xs')
```

## Function: Compose

* Generate a new function that is the composition of two others

```haskell
compose :: (a -> b) -> (b -> c) -> a -> c
compose f g = g . f
```
